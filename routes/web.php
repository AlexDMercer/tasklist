<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
})->name('welcome');

//Auth::routes();
Route::auth();
Route::get('/home', 'HomeController@index')->name('home');

Route::get('/tasks', 'TaskController@index')->name('tasks');
Route::get('/tasks/search', 'TaskController@search')->name('search');
Route::post('/tasks/create', 'TaskController@create')->name('create');
Route::get('/tasks/{id}', 'TaskController@oneTask')->name('one-task');
Route::post('/tasks/{id}', 'TaskController@update')->name('update');
Route::get('/tasks/{id}/delete', 'TaskController@delete')->name('delete');
Auth::routes();


