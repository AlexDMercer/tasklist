<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\TaskRequest;
use App\Task;

class TaskController extends Controller
{
    public function __construct()
    {
    	$this->middleware('auth');
    }

    public function index(Request $request)
    {
        $tasks = $request->user()->tasks()->get();
    	return view('tasks', ['tasks' => $tasks]);
    }

    public function create(TaskRequest $request)
    {
    	//$validation = $request->validate(['name' => 'required|min:3|max:255']);

/*    	$task = new Task();
    	$task->name = $request->input('name');
    	$task->user_id = $request->user('user_id');
    	$task->save(); */

        $request->user()->tasks()->create(['name' => $request->name]);

    	return redirect()->route('tasks')->with('success', 'Задача добавлена');
    }

    public function oneTask($id)
    {
    	$task = new Task;
    	return view('one-task', ['task' => $task::find($id)]);
    }

    public function update($id, TaskRequest $request)
    {
/*    	$task = Task::find($id);
    	$task->name = $request->input('name');
    	$task->user_id = $request->input('user_id');
    	$task->save(); */

        $request->user()->tasks()->find($id)->update(['name' => $request->name]);

    	return redirect()->route('tasks')->with('success', 'Задача отредактирована');
    }

    public function delete($id)
    {
    	$task = Task::find($id)->delete();

    	return redirect()->route('tasks')->with('success', 'Задача удалена');
    }

    public function search(Request $request)
    {
        $search = $request->input('search');
        $tasks = $request->user()->tasks()->where('name', 'LIKE', "%{$search}%")->get();
        return view('search', ['tasks' => $tasks]);
    }
}