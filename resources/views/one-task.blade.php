@extends('layouts.app')

@section('content')
	<div class="card">
	  <div class="card-header">
	    <h4><b>Редактировать задачу</b></h4>
	  </div>
	  <div class="card-body">
	  	<form action="{{route('update', $task->id)}}" method="post">
	  		@csrf
		    <label for="create"><b>{{$task->name}}</b></label>
		    <div class="form-group">
		    	<input type="text" name="name" value="{{$task->name}}" class="form-control">
		    </div>
		    <div class="form-group">
		    	<button type="submit" class="btn btn-warning">редактировать</button>
		    </div>
	    </form>
	  </div>
	</div>
@endsection