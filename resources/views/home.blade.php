@extends('layouts.app')

@section('title')
    Личный кабинет
@endsection

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header"><h3 class="text-center">Личный кабинет</h3></div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                    <div class="container">
                        <div class="card mx-auto" style="width:400px">
                          <img class="card-img-top" src="https://html5css.ru/bootstrap4/img_avatar1.png" alt="Card image">
                          <div class="card-body">
                            <h4 class="card-title text-center">{{$user->name}}</h4>
                            <p class="card-text text-center">{{$user->email}}</p>
                        </div>
                    </div>
                    <br><hr><br>
                    <div class="title m-b-md">
                        <a href="{{route('tasks')}}"><h3><p class="text-justify text-center">Список задач</p></h3></a>
                    </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
