@extends('layouts.app')

@section('content')
<div class="container">
	<div class="row justify-content-center">
        <div class="col-md-8">
			<div class="card">
			  <div class="card-header">
			    <h4><b>Новая задача</b></h4>
			  </div>
			  <div class="card-body">
			  	<form action="{{route('create')}}" method="post">
			  		@csrf
				    <label for="create"><b>Задача</b></label>
				    <div class="form-group">
				    	<input type="text" name="name" placeholder="новая задача" class="form-control">
				    </div>
				    <div class="form-group">
				    	<button type="submit" class="btn btn-primary float-right">добавить</button>
				    </div>
			    </form>
			  </div>
			</div>
		</div>
	</div>
</div>
<br>
<div class="container">
	<div class="row justify-content-center">
        <div class="col-md-8">
			<div class="card">
			  <div class="card-header">
			     <h4><b>Результат поиска:</b></h4>
			  </div>
			  <div class="card-body">
			  	<table class="table table-striped">
			  		@foreach($tasks as $task)
				  		<tr>
				  			<td>{{$task->name}}</td>
				  			<td>
				  				<a href="{{route('delete', $task->id)}}"><button class="btn btn-danger float-right">удалить</button></a>
				  				<a href="{{route('one-task', $task->id)}}"><button class="btn btn-primary float-right">редактировать</button></a>
				  			</td>
				  		</tr>
			  		@endforeach
			  	</table>
			  </div>
			</div>
		</div>
	</div>
</div>
@endsection